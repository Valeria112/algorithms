/*
 * -------------------------------------------------------------------------
 * lists.h
 *	Integrated single and doubly linked lists
 * -------------------------------------------------------------------------
 */

#ifndef LISTS_H
#define LISTS_H

#include <stdbool.h>

/*
 * integer comparator
 */
static inline int icmp(int a, int b)
{
    if (a > b)      return 1;
    else if (a < b) return -1;
    else            return 0;
}

static inline int ricmp(int a, int b)
{
    return icmp(b, a);
}

typedef int(*cmp_prt)(int, int);

/*
 * Lists
 */

typedef enum {
    SINGLY_LINKED_LIST,
    DOUBLY_LINKED_LIST
} list_type;

char *list_print(const void *list, list_type type, bool reverse);

/*
 * Single Linked List
 */

typedef struct slist_node slist_node;
struct slist_node
{
	int data;
    slist_node *next;
};

typedef struct slist
{
    int			length;
    slist_node *head;
    slist_node *tail;
} slist;

#define list_length(list)   ((list)->length)
#define list_is_empty(list) (list_length(list) == 0)

#define foreach(node, list)\
    for ((node) = (list)->head; (node) != NULL; (node) = (node)->next)

slist_node *new_slist_node(int data);

slist *slist_create_empty();

void slist_push_head(slist *list, int data);

void slist_push_tail(slist *list, int data);

slist *slist_copy(const slist *list);

slist *slist_insertion_sort(const slist *list);

void slist_selection_sort(slist *list);

void slist_insert_after(slist *list, slist_node *node, int data);

void slist_delete_first(slist *list);

void slist_delete_next_node(slist *list, slist_node *prev);

void slist_delete_node(slist *list, slist_node *node);

slist_node *slist_find(const slist *list, int data);

void slist_free(slist *list);

bool __slist_is_sorted(const slist *list, cmp_prt cmp);

static inline bool slist_is_sorted(const slist *list, bool reversed)
{
    if (list_is_empty(list)) return true;
    return __slist_is_sorted(list, reversed ? ricmp: icmp);
}

void slist_sorted_insert(slist *list, int data);

static inline char *slist_print(const slist *list)
{
    return list_print((void*) list, SINGLY_LINKED_LIST, false);
}

/*
 * Doubly Linked List
 */

typedef struct dlist_node dlist_node;
struct dlist_node
{
	int data;
    dlist_node *prev;
    dlist_node *next;
};

typedef struct dlist
{
    int			length;
    dlist_node *head;
    dlist_node *tail;
} dlist;

#define foreach_reverse(node, list)\
    for ((node) = (list)->tail; (node) != NULL; (node) = (node)->prev)

dlist_node *new_dlist_node(const int data);

dlist *dlist_create_empty();

void dlist_push_head(dlist *list, int data);

void dlist_push_tail(dlist *list, int data);

dlist *dlist_copy(const dlist *list);

void dlist_insert_after(dlist *list, dlist_node *node, int data);

void dlist_delete_first(dlist *list);

void dlist_delete_last(dlist *list);

void dlist_delete_node(dlist *list, dlist_node *node);

dlist_node *dlist_find(const dlist *list, int data);

void dlist_free(dlist *list);

bool __dlist_is_sorted(const dlist *list, cmp_prt cmp);

static inline bool dlist_is_sorted(const dlist *list, bool reversed)
{
    if (list_is_empty(list)) return true;
    return __dlist_is_sorted(list, reversed ? ricmp : icmp);
}

void dlist_sorted_insert(dlist *list, int data);

static inline char *dlist_print(const dlist *list, bool reverse)
{
    return list_print((void*) list, DOUBLY_LINKED_LIST, reverse);
}


/*
 * Algorithms on custom linked lists
 */

bool slist_check_loop_retracing(slist_node *head, bool break_loop);

slist_node *slist_reverse(slist_node *head);

bool slist_check_loop_reversing(slist_node *head);

bool slist_check_loop_floyd(slist_node *head, bool break_loop);




#endif /* LISTS_H */
