/*
 *-------------------------------------------------------------------------
 * string_builder.c
 *	StringBuilder provides an indefinitely-extensible string data type.
 *-------------------------------------------------------------------------
 */

#include "string_builder.h"

StringBuilder string_builder_create(int size)
{
    StringBuilder builder;
    builder = (StringBuilder) malloc(sizeof(builder));

    string_builder_init(builder, size);

    return builder;
}

void string_builder_init(StringBuilder builder, int size)
{
    if (size < 1) size = MEM_DEFAULT_SIZE;

    builder->capasity  = size;
    builder->buffer    = (char *) malloc(size);

    string_builder_reset(builder);
}

void string_builder_reset(StringBuilder builder)
{
    builder->length    = 0;
    builder->buffer[0] = '\0';
}

void string_builder_free(StringBuilder builder)
{
    free(builder->buffer);
    free(builder);
}

void string_builder_realloc(StringBuilder builder,
                            int length)
{
    int capasity      = builder->capasity * MEM_GROWTH_FACTOR + 1;
    builder->capasity = length > capasity ? length : capasity;

    builder->buffer = (char *) realloc(builder->buffer, builder->capasity);
}

void string_builder_append_string(StringBuilder builder,
                                  char *string,
                                  int slength)
{
    int length = builder->length + slength;

    if (length >= builder->capasity)
        string_builder_realloc(builder, length + 1);

    char *ending = builder->buffer + builder->length;
    for (int i = 0; i < slength; i++, *ending++ = *string++);

    builder->length += slength;
    builder->buffer[builder->length] = '\0';
}

void string_builder_append_format(StringBuilder builder,
                                  const char *format, ...)
{
    va_list	 args;
    char    *buffer;
    int      buffer_free;
    int      n;

    for(;;)
    {
        buffer      = builder->buffer   + builder->length;
        buffer_free = builder->capasity - builder->length;

        va_start(args, format);
        n = vsnprintf(buffer, buffer_free, format, args);
        va_end(args);

        if (n >= buffer_free)
        {
            string_builder_realloc(builder, builder->length + n + 1);
        }

        else if (n < 0)
        {
            perror("encoding error!");
            abort();
        }

        else
        {
            builder->length += n;
            break;
        }
    }

    builder->buffer[builder->length] = '\0';
}
