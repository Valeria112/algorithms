/*
 * -------------------------------------------------------------------------
 * string_builder.h
 *	  Declarations/definitions for "StringBuilder" functions.
 *-------------------------------------------------------------------------
 */

#ifndef STRING_BUILDER_H
#define STRING_BUILDER_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

typedef struct {
    int   length;
    int   capasity;
    char  *buffer;
} StringBuilderData;

typedef StringBuilderData *StringBuilder;

#define MEM_DEFAULT_SIZE  (1024)
#define MEM_GROWTH_FACTOR  (1.5)

#define string_builder_to_string(builder)\
    ((builder)->buffer)

#define string_builder_length(builder)\
    ((builder)->length)

#define string_builder_append_int(builder, val)\
    (string_builder_append_format((builder), "%d", (val)))

StringBuilder string_builder_create(int size);

void string_builder_init(StringBuilder builder, int size);

void string_builder_reset(StringBuilder builder);

void string_builder_free(StringBuilder builder);

void string_builder_realloc(StringBuilder builder,int length);

void string_builder_append_string(StringBuilder builder,
                                  char *string, int length);

void string_builder_append_format(StringBuilder builder,
                                  const char *format, ...);

#endif // STRING_BUILDER_H
