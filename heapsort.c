/*
 *-------------------------------------------------------------------------
 * headsort.c
 *	HeapSort algorithm implementation
 *-------------------------------------------------------------------------
 */
#include <stdio.h>
#include "heapsort.h"


static void swap(int * arr, const uint i, const uint j)
{
    int tmp  = arr[i];
    arr[i] = arr[j];
    arr[j] = tmp;
}


void sift_up(int * arr, uint idx)
{
    while (idx > 0)
    {
        uint p_idx = parent_idx(idx);

        if (arr[p_idx] > arr[idx])
            return;

        swap(arr, p_idx, idx);
        idx = p_idx;
    }
}


void sift_down(int * arr, const uint len)
{
    uint idx = 0;
    while (idx < len)
    {
        uint l_idx, r_idx;
        int next_idx = -1;

        if ((l_idx = child_left_idx(idx)) < len)
            next_idx = l_idx;

        if ((r_idx = child_right_idx(idx)) < len)
            next_idx = next_idx == -1 ? r_idx: max_idx(arr, l_idx, r_idx);

        if (next_idx == -1 || arr[next_idx] <= arr[idx])
            break;

        swap(arr, idx, next_idx);
        idx = next_idx;
    }
}


void heapify(int * arr, const uint len)
{
    for (uint i = 1; i < len; i++)
        sift_up(arr, i);
}


void heapsort(int * arr, const uint len)
{
    if (len < 2)
        return;

    heapify(arr, len);

    uint head_end = len;
    while(head_end--)
    {
        swap(arr, 0, head_end);
        sift_down(arr, head_end);
    }
}
