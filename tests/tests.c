/*
* Unit tests
* use cmocka: https://api.cmocka.org/modules.html
*/
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include "algorithms.h"
#include "lists.h"
#include "string_builder.h"
#include "heapsort.h"

static void test_rand_between(void **state)
{
	for (int i = 0; i < 100; i++)
	{
		assert_in_range(rand_between(0, i), 0, i);
		assert_in_range(rand_between(i, i + 100), i, i + 100);
	}
}

static void test_gcd(void **state)
{
    assert_int_equal(gcd(1, 1), 1);
    assert_int_equal(gcd(1, 2), 1);
    assert_int_equal(gcd(2, 2), 2);
    assert_int_equal(gcd(2, 3), 1);
    assert_int_equal(gcd(4, 6), 2);
    assert_int_equal(gcd(3, 6), 3);
    assert_int_equal(gcd(10, 15), 5);
    assert_int_equal(gcd(49, 70), 7);
    assert_int_equal(gcd(4851, 3003), 231);
}

static void test_lcm(void **state)
{
	assert_int_equal(lcm(1, 1), 1);
	assert_int_equal(lcm(1, 2), 2);
	assert_int_equal(lcm(2, 2), 2);
	assert_int_equal(lcm(3, 5), 15);
	assert_int_equal(lcm(4, 6), 12);
	assert_int_equal(lcm(49, 70), 490);
}

static void test_ipow(void **state)
{
	assert_int_equal(ipow(1, 0), 1);
	assert_int_equal(ipow(1, 1), 1);
	assert_int_equal(ipow(2, 0), 1);
	assert_int_equal(ipow(2, 2), 4);

	assert_int_equal(ipow(2, 8), 256);
	assert_int_equal(ipow(3, 5), 243);
	assert_int_equal(ipow(7, 10), 282475249);
}

static void test_mod_ipow(void **state)
{
	assert_int_equal(mod_ipow(7, 8, 1),   0);
	assert_int_equal(mod_ipow(2, 2, 2),   0);
	assert_int_equal(mod_ipow(3, 3, 5),   2);
	assert_int_equal(mod_ipow(5, 2, 3),   1);
	assert_int_equal(mod_ipow(7, 3, 10),  3);
	assert_int_equal(mod_ipow(10, 9, 17), 7);
}

static void check_factorization(int num, int exp_len, int * expected)
{
	int len;
	int * factors  = factorization(num, &len);
	assert_int_equal(len, exp_len);

	assert_memory_equal((void*) factors, (void*) expected, len * sizeof(int));
	free(factors);
}

static void test_factorization(void **state)
{
	int expected_10[] = { 2, 5 };
	check_factorization(10, 2, expected_10);

	int expected_13[] = { 13 };
	check_factorization(13, 1, expected_13);

	int expected_16[] = { 2, 2, 2, 2 };
	check_factorization(16, 4, expected_16);

	int expected_221[] = { 13, 17 };
	check_factorization(221, 2, expected_221);
}

static void test_find_primes(void **state)
{
	int len;
	int * primes = find_primes(40, &len);

	assert_int_equal(len, 12);

	int expected[] = { 2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37 };
	assert_memory_equal((void*) primes, (void*) expected, 12 * sizeof(int));

	free(primes);
}

static void check_list_string(slist *list, char *expected)
{
    char *got = slist_print(list);
    assert_string_equal(expected, got);
    free(got);
}

static void check_dlist_string(dlist *list,
                               char *expected,
                               char *reversed)
{
    char *got  = dlist_print(list, false);
    char *rgot = dlist_print(list, true);

    assert_string_equal(expected, got);
    assert_string_equal(reversed, rgot);

    free(got);
    free(rgot);
}

static void check_list_empty(slist *list)
{
    assert_int_equal(list_length(list), 0);
    assert_true(list_is_empty(list));
    check_list_string(list, "[ ]");
}

static void check_dlist_empty(dlist *list)
{
    assert_int_equal(list_length(list), 0);
    assert_true(list_is_empty(list));
    check_dlist_string(list, "[ ]", "[ ]");
}

static void test_slist(void **state)
{
    /* empty list */

    slist *list = slist_create_empty();
    check_list_empty(list);

    /* is sorted */
    assert_true(slist_is_sorted(list, false));
    assert_true(slist_is_sorted(list, true));

    /* copy */
    slist *clist = slist_copy(list);
    check_list_empty(clist);

    slist_free(clist);
    free(clist);

    /* push head */

    slist_push_head(list, 5);
    assert_int_equal(list_length(list), 1);
    assert_false(list_is_empty(list));
    check_list_string(list, "[ 5 ]");

    /* is sorted */
    assert_true(slist_is_sorted(list, false));
    assert_true(slist_is_sorted(list, true));

    slist_push_head(list, 7);
    assert_int_equal(list_length(list), 2);
    check_list_string(list, "[ 7 5 ]");

    /* is sorted */
    assert_false(slist_is_sorted(list, false));
    assert_true(slist_is_sorted(list, true));

    /* push tail */

    slist_push_tail(list, 3);
    assert_int_equal(list_length(list), 3);
    check_list_string(list, "[ 7 5 3 ]");

    assert_true(slist_is_sorted(list, true));

    /* selection sort */
    slist_selection_sort(list);
    check_list_string(list, "[ 3 5 7 ]");
    assert_true(slist_is_sorted(list, false));

    slist_free(list);
    check_list_empty(list);

    slist_push_tail(list, 1);
    assert_int_equal(list_length(list), 1);
    check_list_string(list, "[ 1 ]");

    /* insert after */

    slist_insert_after(list, list->head, 9);
    assert_int_equal(list_length(list), 2);
    check_list_string(list, "[ 1 9 ]");

    assert_true(slist_is_sorted(list, false));

    slist_insert_after(list, list->head, 4);
    assert_int_equal(list_length(list), 3);
    check_list_string(list, "[ 1 4 9 ]");

    assert_true(slist_is_sorted(list, false));

    slist_insert_after(list, list->tail, -2);
    assert_int_equal(list_length(list), 4);
    check_list_string(list, "[ 1 4 9 -2 ]");

    /* copy */
    clist = slist_copy(list);
    assert_int_equal(list_length(clist), 4);
    check_list_string(clist, "[ 1 4 9 -2 ]");

    slist_free(clist);
    free(clist);

    assert_false(slist_is_sorted(list, false));
    assert_false(slist_is_sorted(list, true));

    /* insertion sort */
    clist = slist_insertion_sort(list);
    check_list_string(clist, "[ -2 1 4 9 ]");

    assert_true(slist_is_sorted(clist, false));

    slist_free(clist);
    free(clist);

    /* delete first */

    slist_delete_first(list);
    assert_int_equal(list_length(list), 3);
    check_list_string(list, "[ 4 9 -2 ]");

    slist_delete_first(list);
    assert_int_equal(list_length(list), 2);
    check_list_string(list, "[ 9 -2 ]");

    slist_delete_first(list);
    assert_int_equal(list_length(list), 1);
    check_list_string(list, "[ -2 ]");

    slist_delete_first(list);
    check_list_empty(list);

    /* delete next */

    slist_push_tail(list, 1);
    slist_push_tail(list, 2);
    assert_int_equal(list_length(list), 2);

    slist_delete_next_node(list, list->head);
    assert_int_equal(list_length(list), 1);
    check_list_string(list, "[ 1 ]");

    slist_delete_next_node(list, list->tail);
    assert_int_equal(list_length(list), 1);
    check_list_string(list, "[ 1 ]");

    /* delete */

    slist_push_tail(list, 3);
    assert_int_equal(list_length(list), 2);
    check_list_string(list, "[ 1 3 ]");

    slist_delete_node(list, list->head);
    assert_int_equal(list_length(list), 1);
    check_list_string(list, "[ 3 ]");

    slist_push_tail(list, 4);
    slist_push_tail(list, 5);

    assert_int_equal(list_length(list), 3);
    check_list_string(list, "[ 3 4 5 ]");

    slist_delete_node(list, list->head->next);
    assert_int_equal(list_length(list), 2);
    check_list_string(list, "[ 3 5 ]");

    /* sorted insert */
    slist_sorted_insert(list, 0);
    check_list_string(list, "[ 0 3 5 ]");

    slist_sorted_insert(list, 1);
    check_list_string(list, "[ 0 1 3 5 ]");

    slist_sorted_insert(list, 10);
    check_list_string(list, "[ 0 1 3 5 10 ]");

    slist_sorted_insert(list, 10);
    check_list_string(list, "[ 0 1 3 5 10 10 ]");

    slist_sorted_insert(list, 0);
    check_list_string(list, "[ 0 0 1 3 5 10 10 ]");

    slist_sorted_insert(list, 3);
    check_list_string(list, "[ 0 0 1 3 3 5 10 10 ]");

    slist_free(list);
    free(list);

    /* find */

    slist *data = slist_create_empty();
    slist_push_head(data, 3);
    slist_push_head(data, 2);
    slist_push_head(data, -1);
    check_list_string(data, "[ -1 2 3 ]");

    slist_node *node = slist_find(data, 3);
    assert_true(data->tail == node);
    assert_int_equal(node->data, 3);

    node = slist_find(data, -1);
    assert_true(data->head == node);
    assert_int_equal(node->data, -1);

    node = slist_find(data, 10);
    assert_true(node == NULL);

    slist_free(data);
    free(data);
}

static void test_dlist(void **state)
{
    /* empty list */

    dlist *list = dlist_create_empty();
    check_dlist_empty(list);

    /* copy */
    dlist *clist = dlist_copy(list);
    check_dlist_empty(clist);

    dlist_free(clist);
    free(clist);

    /* check sorted */
    assert_true(dlist_is_sorted(list, false));
    assert_true(dlist_is_sorted(list, true));

    /* push_head */

    dlist_push_head(list, 5);
    assert_int_equal(list_length(list), 1);
    assert_false(list_is_empty(list));
    check_dlist_string(list, "[ 5 ]", "[ 5 ]");

    /* check sorted */
    assert_true(dlist_is_sorted(list, false));
    assert_true(dlist_is_sorted(list, true));

    dlist_push_head(list, 7);
    assert_int_equal(list_length(list), 2);
    check_dlist_string(list, "[ 7 5 ]", "[ 5 7 ]");

    /* check sorted */
    assert_false(dlist_is_sorted(list, false));
    assert_true(dlist_is_sorted(list, true));

    /* push tail */

    dlist_push_tail(list, 3);
    assert_int_equal(list_length(list), 3);
    check_dlist_string(list, "[ 7 5 3 ]", "[ 3 5 7 ]");

    /* check sorted */
    assert_false(dlist_is_sorted(list, false));
    assert_true(dlist_is_sorted(list, true));

    dlist_free(list);
    check_dlist_empty(list);

    dlist_push_tail(list, 1);
    assert_int_equal(list_length(list), 1);
    check_dlist_string(list, "[ 1 ]", "[ 1 ]");

    /* insert after */

    dlist_insert_after(list, list->head, 9);
    assert_int_equal(list_length(list), 2);
    check_dlist_string(list, "[ 1 9 ]", "[ 9 1 ]");

    dlist_insert_after(list, list->head, 4);
    assert_int_equal(list_length(list), 3);
    check_dlist_string(list, "[ 1 4 9 ]", "[ 9 4 1 ]");

    dlist_insert_after(list, list->tail, -2);
    assert_int_equal(list_length(list), 4);
    check_dlist_string(list, "[ 1 4 9 -2 ]", "[ -2 9 4 1 ]");

    /* copy */
    clist = dlist_copy(list);
    assert_int_equal(list_length(clist), 4);
    check_dlist_string(clist, "[ 1 4 9 -2 ]", "[ -2 9 4 1 ]");

    dlist_free(clist);
    free(clist);

    /* check sorted */
    assert_false(dlist_is_sorted(list, false));
    assert_false(dlist_is_sorted(list, true));

    /* delete first */

    dlist_delete_first(list);
    assert_int_equal(list_length(list), 3);
    check_dlist_string(list, "[ 4 9 -2 ]", "[ -2 9 4 ]");

    dlist_delete_first(list);
    assert_int_equal(list_length(list), 2);
    check_dlist_string(list, "[ 9 -2 ]", "[ -2 9 ]");

    /* delete last */

    dlist_delete_last(list);
    assert_int_equal(list_length(list), 1);
    check_dlist_string(list, "[ 9 ]", "[ 9 ]");

    dlist_delete_last(list);
    check_dlist_empty(list);

    /* delete */

    dlist_push_head(list, 1);
    dlist_push_tail(list, 3);

    dlist_delete_node(list, list->head);
    assert_int_equal(list_length(list), 1);
    check_dlist_string(list, "[ 3 ]", "[ 3 ]");

    dlist_push_tail(list, 4);
    dlist_push_tail(list, 5);

    dlist_delete_node(list, list->head->next);
    assert_int_equal(list_length(list), 2);
    check_dlist_string(list, "[ 3 5 ]", "[ 5 3 ]");

    dlist_delete_node(list, list->tail);
    assert_int_equal(list_length(list), 1);
    check_dlist_string(list, "[ 3 ]", "[ 3 ]");

    /* sorted insert */
    dlist_sorted_insert(list, 0);
    check_dlist_string(list, "[ 0 3 ]", "[ 3 0 ]");

    dlist_sorted_insert(list, 5);
    check_dlist_string(list, "[ 0 3 5 ]", "[ 5 3 0 ]");

    dlist_sorted_insert(list, 4);
    check_dlist_string(list, "[ 0 3 4 5 ]", "[ 5 4 3 0 ]");

    dlist_sorted_insert(list, 0);
    check_dlist_string(list, "[ 0 0 3 4 5 ]", "[ 5 4 3 0 0 ]");

    dlist_sorted_insert(list, 5);
    check_dlist_string(list, "[ 0 0 3 4 5 5 ]", "[ 5 5 4 3 0 0 ]");

    dlist_sorted_insert(list, 3);
    check_dlist_string(list, "[ 0 0 3 3 4 5 5 ]", "[ 5 5 4 3 3 0 0 ]");

    dlist_free(list);
    free(list);

    /* find */

    dlist *data = dlist_create_empty();
    dlist_push_head(data, 3);
    dlist_push_head(data, 2);
    dlist_push_head(data, -1);

    dlist_node *node = dlist_find(data, 3);
    assert_true(data->tail == node);
    assert_int_equal(node->data, 3);

    node = dlist_find(data, -1);
    assert_true(data->head == node);
    assert_int_equal(node->data, -1);

    node = dlist_find(data, 10);
    assert_true(node == NULL);

    dlist_free(data);
    free(data);
}

static void test_list_loop(void **state)
{
    /*
     *      0--1
     *      |  |
     *      3--2
     */
    slist_node *head             = new_slist_node(0);
    head->next                   = new_slist_node(1);
    head->next->next             = new_slist_node(2);
    head->next->next->next       = new_slist_node(3);
    head->next->next->next->next = head;

    assert_true(slist_check_loop_retracing(head, false));
    assert_true(slist_check_loop_reversing(head));
    assert_true(slist_check_loop_floyd(head, false));

    /*
     *      0--1--2
     *         |  |
     *         4--3
     */
    head->next->next->next->next       = new_slist_node(4);
    head->next->next->next->next->next = head->next;

    assert_true(slist_check_loop_retracing(head, false));
    assert_true(slist_check_loop_reversing(head));
    assert_true(slist_check_loop_floyd(head, false));

    /*
     * 4--2
     */
    head->next->next->next->next->next = head->next->next;

    assert_true(slist_check_loop_retracing(head, false));
    assert_true(slist_check_loop_reversing(head));
    assert_true(slist_check_loop_floyd(head, false));

    /*
     *     7--0--1--2
     *         \    |
     *           4--3
     */
    head->next->next->next->next->next = head;
    slist_node *new_head               = new_slist_node(7);
    new_head->next                     = head;

    assert_true(slist_check_loop_retracing(head, false));
    assert_true(slist_check_loop_reversing(head));
    assert_true(slist_check_loop_floyd(head, false));

    /* break loop */
    slist_check_loop_retracing(new_head, true);

    assert_false(slist_check_loop_retracing(new_head, false));
    assert_false(slist_check_loop_reversing(new_head));

    assert_null(head->next->next->next->next->next);

    head->next->next->next->next->next = head;
    assert_true(slist_check_loop_floyd(head, true));
    assert_null(head->next->next->next->next->next);
}

static void test_string_builder(void ** state)
{
    /* empty */

    StringBuilder builder = string_builder_create(10);

    assert_string_equal(builder->buffer, "");
    assert_int_equal(builder->length, 0);
    assert_int_equal(builder->capasity, 10);

    /* append int */

    string_builder_append_int(builder, 17);

    assert_string_equal(builder->buffer, "17");
    assert_int_equal(builder->length, 2);
    assert_int_equal(string_builder_length(builder), 2);
    assert_int_equal(builder->capasity, 10);

    string_builder_append_int(builder, 171717177);

    assert_string_equal(builder->buffer, "17171717177");
    assert_string_equal(string_builder_to_string(builder), "17171717177");
    assert_int_equal(builder->length, 11);
    assert_int_equal(builder->capasity, 16);

    /* reset */

    string_builder_reset(builder);

    assert_string_equal(builder->buffer, "");
    assert_int_equal(builder->length, 0);
    assert_int_equal(builder->capasity, 16);

    /* append string */

    string_builder_append_string(builder, "hello", 3);
    assert_string_equal(builder->buffer, "hel");
    assert_int_equal(builder->length, 3);

    string_builder_append_string(builder, "lo world !!! aaabbb", 19);
    assert_string_equal(builder->buffer, "hello world !!! aaabbb");
    assert_int_equal(builder->length, 22);
    assert_int_equal(builder->capasity, 25);

    /* realloc */

    string_builder_realloc(builder, 45);
    assert_int_equal(builder->capasity, 45);

    string_builder_realloc(builder, 60);
    assert_int_equal(builder->capasity, 68);

    /* free */

    string_builder_free(builder);

    /* append format */

    builder = string_builder_create(10);
    string_builder_append_format(builder, "%d - (%d) %s", 12, -17, "= ?");
    assert_string_equal(builder->buffer, "12 - (-17) = ?");
    assert_int_equal(builder->length, 14);
    assert_int_equal(builder->capasity, 16);

    string_builder_append_format(builder, " %s ", "I am a string. My len is 28.");
    assert_string_equal(builder->buffer, "12 - (-17) = ? I am a string. My len is 28. ");
    assert_int_equal(builder->length, 44);
    assert_int_equal(builder->capasity, 45);

    string_builder_free(builder);

    /* mixed */

    builder = string_builder_create(10);
    string_builder_append_format(builder, "%d ",  10);
    string_builder_append_format(builder, "%d ",  20);
    string_builder_append_string(builder, "30 ",  3);
    string_builder_append_string(builder, "40 0", 3);
    string_builder_append_int(builder, 50);

    assert_string_equal(string_builder_to_string(builder), "10 20 30 40 50");
    assert_int_equal(builder->length,   14);
    assert_int_equal(builder->capasity, 16);

    string_builder_free(builder);
}

static void check_sift_up(int * data, int * heap, int len)
{
    sift_up(data, len - 1);
    assert_memory_equal(data, heap, len * sizeof(int));
}

static void check_sift_down(int * data, int * heap, int len)
{
    sift_down(data, len);
    assert_memory_equal(data, heap, len * sizeof(int));
}

static void check_heapify(int * data, int * heap, int len)
{
    heapify(data, len);
    assert_memory_equal(data, heap, len * sizeof(int));
}

static void check_heapsort(int * data, int len)
{
    heapsort(data, len);

    for (int i = 0; i < len - 1; i++)
        assert_false(data[i] > data[i + 1]);
}

static void arr_display(int * arr, int len)
{
    for (int i = 0; i < len; i++)
        printf("%d ", arr[i]);
    printf("\n");
}

static void check_heapsort_random_data()
{
    const int size = 50;

    int arr[size];
    for (int i = 0; i < size; i++)
        arr[i] = rand_between(-10, 100);

    printf("Generated Data: ");
    arr_display(arr, size);

    check_heapsort(arr, size);

    printf("Sorted Data: ");
    arr_display(arr, size);
}

static void test_heapsort(void ** state)
{
    assert_int_equal(child_left_idx(0),  1);
    assert_int_equal(child_right_idx(0), 2);

    assert_int_equal(child_left_idx(1),  3);
    assert_int_equal(child_right_idx(1), 4);

    assert_int_equal(child_left_idx(2),  5);
    assert_int_equal(child_right_idx(2), 6);

    assert_int_equal(parent_idx(1), 0);
    assert_int_equal(parent_idx(2), 0);

    assert_int_equal(parent_idx(5), 2);
    assert_int_equal(parent_idx(6), 2);

    assert_int_equal(parent_idx(7), 3);
    assert_int_equal(parent_idx(8), 3);

    int max_heap[] = {10, 7, 5, 4, 3, 8};
    int got_heap[] = {10, 7, 8, 4, 3, 5};
    check_sift_up(max_heap, got_heap, 6);

    int max_heap1[] = {8, 7, 4, 3, 9};
    int got_heap1[] = {9, 8, 4, 3, 7};
    check_sift_up(max_heap1, got_heap1, 5);

    int data[] = {};
    int heap[] = {};
    check_heapify(data, heap, 0);

    int data1[] = {1};
    int heap1[] = {1};
    check_heapify(data1, heap1, 1);

    int data2[] = {1, 2};
    int heap2[] = {2, 1};
    check_heapify(data2, heap2, 2);

    int data3[] = {5, 8, 4, 11, 12, 2, 3};
    int heap3[] = {12, 11, 4, 5, 8, 2, 3};
    check_heapify(data3, heap3, 7);

    int data4[] = {1, 9, 2, 8, 3, 7, 4, 6, 5, 5};
    int heap4[] = {9, 8, 7, 6, 5, 2, 4, 1, 5, 3};
    check_heapify(data4, heap4, 10);

    check_sift_down(data, heap, 0);
    check_sift_down(data1, heap1, 1);

    int data5[] = {1, 2};
    int heap5[] = {2, 1};
    check_sift_down(data5, heap5, 2);

    int data6[] = {6, 7, 8};
    int heap6[] = {8, 7, 6};
    check_sift_down(data6, heap6, 3);

    int data7[] = {6, 9, 5, 8, 4, 3, 4, 2, 1, 3, 2};
    int heap7[] = {9, 8, 5, 6, 4, 3, 4, 2, 1, 3, 2};
    check_sift_down(data7, heap7, 11);

    check_heapsort(data, 0);
    check_heapsort(data1, 1);
    check_heapsort(data2, 2);
    check_heapsort(data3, 7);
    check_heapsort(data4, 10);
    check_heapsort(data6, 3);
    check_heapsort(data7, 11);

    check_heapsort_random_data();
    check_heapsort_random_data();
}

int main()
{
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_gcd),
        cmocka_unit_test(test_lcm),
        cmocka_unit_test(test_ipow),
        cmocka_unit_test(test_mod_ipow),
        cmocka_unit_test(test_rand_between),
        cmocka_unit_test(test_factorization),
        cmocka_unit_test(test_find_primes),
        cmocka_unit_test(test_slist),
        cmocka_unit_test(test_dlist),
        cmocka_unit_test(test_list_loop),
        cmocka_unit_test(test_string_builder),
        cmocka_unit_test(test_heapsort)
    };

    return cmocka_run_group_tests(tests, NULL, NULL);
}
