/*
 *-------------------------------------------------------------------------
 * lists.c
 *	Algorithms with linked lists implementation
 *-------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <limits.h>
#include <string.h>

#include "lists.h"
#include "string_builder.h"


char *list_print(const void *list, list_type type, bool reverse)
{
    slist_node *scurrent;
    dlist_node *dcurrent;

    StringBuilder  builder = string_builder_create(10);
    string_builder_append_string(builder, "[ ", 2);

    switch (type) {

        case SINGLY_LINKED_LIST:
            foreach (scurrent, (slist*) list)
                    string_builder_append_format(builder, "%d ", scurrent->data);
            break;

        case DOUBLY_LINKED_LIST:
            if (reverse)
            {
                foreach_reverse (dcurrent, (dlist*) list)
                    string_builder_append_format(builder, "%d ", dcurrent->data);
            }
            else
            {
                foreach (dcurrent, (dlist*) list)
                    string_builder_append_format(builder, "%d ", dcurrent->data);
            }
            break;
    };

    string_builder_append_string(builder, "]", 1);

    char *ret = string_builder_to_string(builder);
    free(builder);

    return ret;
}

/* SINGLE LINKED LIST */

/*
 * create and fill list node
 */
slist_node *new_slist_node(int data)
{
    slist_node *node = (slist_node*) malloc(sizeof(slist_node));
    node->data = data;
    node->next = NULL;

    return node;
}

/*
 * create empty single linked list
 */
slist *slist_create_empty()
{
    slist *list = (slist*) malloc(sizeof(slist));
    list->head = list->tail = NULL;
    list->length = 0;

    return list;
}

/*
 * insert list node to the begin of the list
 */
void slist_push_head(slist *list, int data)
{
    slist_node *node = new_slist_node(data);

    if (list_is_empty(list))
    {
        list->head = list->tail = node;
    }
    else
    {
        node->next = list->head;
        list->head = node;
    }

    list->length++;
}

/*
 * insert list node to the end of the list
 */
void slist_push_tail(slist *list, int data)
{
    slist_node * node = new_slist_node(data);

    if (list_is_empty(list))
    {
        list->head = list->tail = node;
    }
    else
    {
        list->tail->next = node;
        list->tail = node;
    }

    list->length++;
}

/*
 * return copy of list
 */
slist *slist_copy(const slist *list)
{
    slist_node *node;
    slist      *copy_list = slist_create_empty();

    foreach (node, list)
        slist_push_tail(copy_list, node->data);

    return copy_list;
}

/*
 * return sorted copy of list
 */
slist *slist_insertion_sort(const slist *list)
{
    slist_node *node;
    slist      *copy_list = slist_create_empty();

    foreach (node, list)
        slist_sorted_insert(copy_list, node->data);

    return copy_list;
}

/*
 * find list node with minimum data
 * starting from begin node
 */
slist_node *slist_min(slist_node *begin)
{
    slist_node *min = begin;

    for (begin = begin->next; begin; begin = begin->next)
    {
        if (begin->data < min->data)
            min = begin;
    }

    return min;
}

/*
 * change data in two list nodes
 */
void slist_swap(slist_node *a, slist_node *b)
{
    int tmp = a->data;
    a->data = b->data;
    b->data = tmp;
}

/*
 * sort list using selection sort method
 *  without list copy
 */
void slist_selection_sort(slist *list)
{
    if (list_length(list) < 2)
        return;

    slist_node *min;

    for (slist_node *tmp = list->head; tmp->next; tmp = tmp->next)
    {
        min = slist_min(tmp);

        if (min->data != tmp->data)
            slist_swap(tmp, min);
    }
}

/*
 * insert new node after list node
 */
void slist_insert_after(slist *list, slist_node *node, int data)
{
    if (node == list->tail)
        return slist_push_tail(list, data);

    slist_node * new_node = new_slist_node(data);

    new_node->next = node->next;
    node->next = new_node;

    list->length++;
}

/*
 * delete first list node
 */
void slist_delete_first(slist *list)
{
    if (list_length(list) < 2)
        return slist_free(list);

    slist_node *head = list->head;
    list->head = list->head->next;

    list->length--;
    free(head);
}

/*
 * delete list node using previous node
 */
void slist_delete_next_node(slist *list, slist_node *prev)
{
    if (prev == list->tail)
        return;

    slist_node* current = prev->next;
    if (current == list->tail)
        list->tail = prev;

    prev->next = prev->next->next;

    list->length--;
    free(current);
}

/*
 * delete list node using next node
 */
void slist_delete_node(slist *list, slist_node *node)
{
    if (node == list->head)
        return slist_delete_first(list);

    assert(node->next != NULL);
    node->data = node->next->data;

    slist_delete_next_node(list, node);
}

/*
 * find first node with node->data equal to data
 */
slist_node *slist_find(const slist *list, int data)
{
    slist_node *current;

    foreach (current, list)
    {
        if (current->data == data)
            return current;
	}

	return NULL;
}

/*
 * free memory and reset list
 */
void slist_free(slist *list)
{
    slist_node *current = list->head;

    while (current)
    {
        slist_node *next = current->next;
        free(current);
        current = next;
	}

    list->head = list->tail = NULL;
    list->length = 0;
}

/*
 * return true if list is sorted (or empty)
 * and false otherwise
 */
bool __slist_is_sorted(const slist *list, cmp_prt cmp)
{
    slist_node *current = list->head;
    while (current->next)
    {
        if (cmp(current->data, current->next->data) == 1)
            return false;

        current = current->next;
    }

    return true;
}

/*
 * insert data into sorted list
 * maintaining list sorted
 */
void slist_sorted_insert(slist *list, int data)
{
    if (list_is_empty(list) || data < list->head->data)
        return slist_push_head(list, data);

    if (data > list->tail->data)
        return slist_push_tail(list, data);

    slist_node *current = list->head;
    while (current->next)
    {
        if (current->next->data > data)
            return slist_insert_after(list, current, data);

        current = current->next;
    }

    // tail->data == data
    slist_push_tail(list, data);
}

/* DOUBLY LINKED LIST */

dlist_node *new_dlist_node(const int data)
{
    dlist_node *node;
    node = (dlist_node*) malloc(sizeof(node));

    node->data = data;
    node->prev = node->next = NULL;

    return node;
};

dlist *dlist_create_empty()
{
    dlist *list  = (dlist*) malloc(sizeof(dlist));
    list->head   = list->tail = NULL;
    list->length = 0;

    return list;
}

void dlist_push_head(dlist *list, int data)
{
    dlist_node *node = new_dlist_node(data);

    if (list_is_empty(list))
    {
        list->head = list->tail = node;
    }
    else
    {
        node->next = list->head;
        list->head->prev = node;
        list->head = node;
    }

    list->length++;
}

void dlist_push_tail(dlist *list, int data)
{
    dlist_node * node = new_dlist_node(data);

    if (list_is_empty(list))
    {
        list->head = list->tail = node;
    }
    else
    {
        list->tail->next = node;
        node->prev = list->tail;
        list->tail = node;
    }

    list->length++;
}

dlist *dlist_copy(const dlist *list)
{
    dlist_node *node;
    dlist      *copy_list = dlist_create_empty();

    foreach (node, list)
        dlist_push_tail(copy_list, node->data);

    return copy_list;
}

void dlist_insert_after(dlist *list, dlist_node *node, int data)
{
    if (node == list->tail)
        return dlist_push_tail(list, data);

    dlist_node * new_node = new_dlist_node(data);

    new_node->next = node->next;
    new_node->prev = node;

    node->next = new_node;
    new_node->next->prev = new_node;

    list->length++;
}

void dlist_delete_first(dlist *list)
{
    if (list_length(list) < 2)
        return dlist_free(list);

    dlist_node *head = list->head;
    list->head = head->next;
    list->head->prev = NULL;

    list->length--;
    free(head);
}

void dlist_delete_last(dlist *list)
{
    if (list_length(list) < 2)
        return dlist_free(list);

    dlist_node *last = list->tail;

    list->tail = last->prev;
    free(last);

    list->tail->next = NULL;
    list->length--;
}

void dlist_delete_node(dlist *list, dlist_node *node)
{
    if (node == list->head)
        return dlist_delete_first(list);

    if (node == list->tail)
        return dlist_delete_last(list);

    node->prev->next = node->next;
    node->next->prev = node->prev;

    list->length--;
	free(node);
}

dlist_node *dlist_find(const dlist *list, int data)
{
    dlist_node *current;

    foreach (current, list) {
        if (current->data == data)
            return current;
    }

	return NULL;
}

void dlist_free(dlist *list)
{
    dlist_node *next;
    dlist_node *current = list->head;

    while (current)
    {
        next = current->next;
        free(current);
        current = next;
	}

    list->head = list->tail = NULL;
    list->length = 0;
}

bool __dlist_is_sorted(const dlist *list, cmp_prt cmp)
{
    dlist_node *current = list->head;
    while (current->next)
    {
        if (cmp(current->data, current->next->data) == 1)
            return false;

        current = current->next;
    }

    return true;
}

void dlist_sorted_insert(dlist *list, int data)
{
    if (list_is_empty(list) || data < list->head->data)
        return dlist_push_head(list, data);

    if (data > list->tail->data)
        return dlist_push_tail(list, data);

    dlist_node *current;
    foreach (current, list)
    {
        if (current->data > data)
            return dlist_insert_after(list, current->prev, data);
    }

    // tail->data == data
    dlist_push_tail(list, data);
}


/*
 * Custom linked lists with loops
 * https://en.wikipedia.org/wiki/Cycle_detection
 */

/*
 * check loop and break it if find
 * using repeatable tracing method
 */
bool slist_check_loop_retracing(slist_node *head, bool break_loop)
{
    slist_node *tmp = head;
    slist_node *tracer;

    while (tmp->next)
    {
        tracer = head;
        while (tracer != tmp)
        {
            if (tracer == tmp->next) {
                if (break_loop)
                    tmp->next = NULL;

                return true;
            }

            tracer = tracer->next;
        }

        tmp = tmp->next;
    }

    return false;
}

/*
 * reverse linked list
 * return new head
 */
slist_node *slist_reverse(slist_node *head)
{
    slist_node *prev = NULL;
    slist_node *curr = head;
    slist_node *next;

    while (curr)
    {
        next = curr->next;
        curr->next = prev;

        prev = curr;
        curr = next;
    }

    return prev;
}

/*
 * check loop using reversing method
 */
bool slist_check_loop_reversing(slist_node *head)
{
    if (!head->next)
        return false;

    slist_node *new_head = slist_reverse(head);

    slist_reverse(new_head);

    return new_head == head;
}

/*
 * check loop and break it if fing
 * using Floyd's algorithm also named as "tortoise and hare"
 */
bool slist_check_loop_floyd(slist_node *head, bool break_loop)
{
    slist_node *tortoise = head;
    slist_node *hare     = head;

    // the hare speed is two cell per node
    // the tortoise speed is one cell per node
    while (1)
    {
        // no loop detected
        if (!hare->next && !hare->next->next)
            return false;

        hare     = hare->next->next;
        tortoise = tortoise->next;

        // the hare and the tortoise in the same cell
        if (hare == tortoise)
            break;
    }

    // here loop detected
    if (!break_loop)
        return true;

    // place hare to the start
    hare = head;

    // now the hare speed is one cell per node
    while (1)
    {
        // the hare and the tortoise in the same cell again
        // they are at the beginig of the loop now
        if (hare == tortoise)
        {
            // hare stopped and tortoise is going to find hare
            while (tortoise->next != hare)
                tortoise = tortoise->next;

            // break the loop and exit
            tortoise->next = NULL;
            return true;
        }

        hare     = hare->next;
        tortoise = tortoise->next;
    }
}
